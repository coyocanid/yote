What is yote
  * Yote is a platform for writing client/server apps.
  * It has a JSON RPC protocol to bind javascript objects to perl objects.
  * Yote organizes the perl objects in a rooted tree structure
  * Yote automatically tracks and persists the object state with an object store.
  * The Yote server knows the state of the client.
  * The Yote server only performs calls it knows the client is allowed to make.
  * Client object states are updated with every app call.
  * Methods written on the server objects automatically appear on the client objects
  * Client object methods return promises that resolve to the return values of the server object methods

What do you need to know to use yote
  * How to use YAML : https://learnxinyminutes.com/docs/yaml
  * How to get the webserver to use modperl or perl cgi
  * Simple perl
  * perl OOP basics
  * javascript ec6
  * javascript promises

What do you write with yote
  * A simple configuration
  * An object model
  * Server side packages and methods
  * The UI

What do you NOT write with yote
  * Database Schemas
  * ajax protocols
  
Design Philosophy

  * Simple to install
  * Configuration options are minimal and in one place
  * Names are descriptive
  * dependencies are kept to a minimum
  * No more than one instance of a yote object with a particular id

=========== Example Apps ===========

Learn yote in a few examples.

page counter, login counter, upload file

1) simple page counter

  server config : /opt/yote/yote-conf.yaml (https://learnxinyminutes.com/docs/yaml/)

DOMAINS:
  'madyote.com':
    DATA-STORE:
      CLASS: Data::RecordStore
      DIRECTORY: /opt/yote/data_store
    ENDPOINT: madyote.com/cgi-bin/endpoint.cgi
    APPS:
      pageCounter: 
        MAIN-APP-CLASS: 'MyApp::PageCounter'
        APP-METHODS:
          'MyApp::PageCounter':
            PUBLIC:
              - update_counter
              - fetch

In the datastore section, the CLASS field will allways be there.
The other fields are options to configure that class.
   DATA-STORE:
     CLASS: Data::Recordstore::MySQL
     HOST: somesqlhost
     PORT: some port number
     DB: database
     USER: username
     PASSWORD: plaintext ( vault reference in the future )

   DATA-STORE:
     CLASS: Data::Recordstore::Mongo
     HOST: somesqlhost
     PORT: some port number
     DB: database
     USER: username
     PASSWORD: plaintext ( vault reference in the future )

   DATA-STORE:
     CLASS: Data::Recordstore::Sharded
     HOST: somesqlhost
     PORT: some port number
     DB: database
     USER: username
     PASSWORD: plaintext ( vault reference in the future )

  server code : /opt/yote/lib/MyApp/PageCouner
  
     package MyApp::PageCounter;
     
     use strict;       
     use warnings;
     
     use Yote::App;
     use base 'Yote::App';

     sub update_counter {
         my( $self ) = @_;
         
         # updates to a page are done one at a time
         $self->lock( "PAGECOUNTER" );
         
         return 1,  # <---- 1 for success, 0 for failure
                $self->set_hits( 1 + $self->get_hits(0) ); 
     }
     sub fetch {
         my( $self ) = @_;
         return 1, $self;
     }
     
     1;

Notes :
  * All app initiators must be a descendant of Yote::App
  * app methods return two values.
      - 1 for success or 0 for failure
      - value to return or error message
  * return values may be from the following
      - string, number or undefined
      - hash reference
      - array reference
      - blessed object that inherits from Yote::Obj

  client code : /var/www/html/index.html
     <!DOCTYPE html>
     <http>
        <head>
        </head>
        <body>
          <main>
            <h1>Welcome to my site. It has been viewed <span id="count"> times</h1>
          </main>
        </body>
        <script>
          <script type="module" src="/yote/js/yote.js"></script>
          fetchYote( 'pagecounter' )
             .then( counterApp => 
                    // all app methods are promises
                    counterApp
                      .update_counter()
                      .then( count => document.getElementById( 'count' ).textContent = count )
                  );
        </script>
     </http>

2) advanced page counter
  server side :
  
     package App::PageCounter;

     #
     # This keeps track of the number of hits a page is getting.
     #
     
     use strict;       
     use warnings;
     
     use App::Base;
     use base 'App::Base';

     sub update_counter {
         my( $self, $page_name ) = @_;
         my $ip = $ENV{IP};
         
         # updates to a page are done one at a time
         $self->lock( "PAGE_$page_name" );
         
         # get_pages (any get_foo)
         # takes a single argument that is the default value if foo is undefined
         my $pages = $self->get_pages({});
         my $page = $pages->{$page_name};
         unless( $page ) {
             # yote objects are created with the create_container method
             # all container objects have a method that returns their datastore
             $page = $self->store->create_container;
             $pages->{$page_name} = $page;
         }
         
         my $from = $ENV{FROM}; # client info
         
         $page->set_total_count( 1 + $page->get_total_count(0) );
         $page->get__ip_hits({})->{$ip}++;
         $page->set_ip_count( keys %{$page->get__ip_hits} );

         # keep track of bot traffic too
         if( $from =~ /bot/i ) {
             $page->get__ip_bot_hits({})->{$ip}++;
             $page->set_bot_count( 1 + $page->get_bot_count(0) );
             $page->set_ip_bot_count( keys %{$page->get__ip_hits} );   
         }
         
         return $page;
         # state is automatically saved
         # locks are automatically unlocked
         # the page object is returned to the client
     }
     1;

  client side
     <!DOCTYPE html>
     <http>
        <head>
          <script type="module" src="/js/yote.js"></script>
        </head>
        <body>
          <main>
            <h1>Welcome to my main page</h1>
          </main>
          <footer>
            Total hits <span id="total_count">.
            Number of distinct ip hits <span id="ip_count">.
            Number of bot hits <span id="bot_count">.
            Number of bot ips <span id="ip_bot_count">.
          </footer>
        </body>
        <script>
          let counter = YoteApp( 'App::PageCounter' );
          // all app methods are promises
          counter.update_counter( 'main_page' )
             .then( page => // return value of the method was the page
                 [ 'total_count', 'ip_count', 'bot_count', 'ip_bot_count' ]
                  .forEach( ct => byId( ct ).textContent = page.get(ct) )
                  );
        </script>
     </http>


3) login counter
  server side :
  
     package App::LoginCounter;

     #
     # This keeps track of the number of times an account has logged in.
     #
     
     use strict;       
     use warnings;
     
     use App::Base;
     use base 'App::Base';

     sub update_login_counter {
         # session is automatically provided via login
         my( $self, undef, $session ) = @_;

         my $acct = $session->get_account;

         # updates to a page are done one at a time
         $self->lock( "LOGINCOUNT_$acct" );

         # set returns the value that it was set to
         return $acct->set_login_count( 1 + $acct->get_login_count(0) );
     }
     1;

     <!DOCTYPE html>
     <http>
        <head>
          <script type="module" src="/js/yote.js"></script>
        </head>
        <body>
          <main>
            <h1>Welcome to my main page</h1>
            <div id="logged_in" style="display:none">
               Login Name <input type="text" id="login_name">
               Password <input type="password" id="password">
            </div>
            <div id="logged_in" style="display:none">
               You have logged in <span id="login_count"></span> times.
            </div>
          </main>
        </body>
        <script>
          let counter = YoteApp( 'App::PageCounter' );
          // all app methods are promises
          counter.update_counter( 'main_page' )
             .then( page => // return value of the method was the page
                 [ 'total_count', 'ip_count', 'bot_count', 'ip_bot_count' ]
                  .forEach( ct => byId( ct ).textValue = page.get(ct) )
                  );
        </script>
     </http>

=========== Intro ===========

Yote is platform to build client/server apps that
communicate with an HTTP endpoint with a common
protocol; the app state is automatically persisted
into an object store rather than a database.

Yote Apps :
  * are written on the server side in perl
  * are written on the client side in javascript
  * are composed of objects
  * automatically connect client calls
    to server side methods    
  * the server objects know the state of the client
  * server objects and client objects are mirrored
  * client objects are automatically updated in every
    call to the server
  * need no database schema
  * do not depend on any javascript libraries
  * work well with Vue.js and provide Vue.js widgets

Yote design philosophy :
 "Focus, focus"
    Let the developer focus on logic and structure
    rather than protocols, layers, the file system
    or persistance.
    
 "Aw, just do it"
    Connecting and reconnecting yote objects should be
    as easy as connecting and reconnecting tinker toys.

 "Control your endpoints"
    Objects are passed as parameters between client and
    server. The server keeps track of all objects it has
    passed to the client

 Ip Man quote about using a technique with very few parts
 Know a few things very well.
    Have a minimal configuration    
    Write methods on the app objects
    Know when to use locking to coordinate objects

Yote is dedicated to the memory of Dr David Dietz.
He taught me far more than he ever realized.

This document describes the yote app server.
It starts with showing a working apache config
before delving into the components.

The following skill are needed :
  * locking
  * object heirarchies, xpath
  * writing the UI
  * writing methods on the app
  * using vol

=========== Boostrapping manually ===========

$ sudo mkdir /opt/yote
$ cd /opt/yote
$ chown wolf.apache .
$ chmod 775 .
$ mkdir lib
$ cd lib
$ ln -s ~/open_source/yote/lib/*pm ~/open_source/yote/lib/App .
$ cd /var/www/cgi-bin
$ sudo ln -s ~/open_source/yote/cgi-bin/adapter.cgi .
$ cd /var/www/html
$ sudo ln -s ~/open_source/yote/html yote
$ sudo yum -i mod_perl
$ edit /etc/http/conf.d/perl.conf with stanza below
$ 

=========== Boostrapping with  /opt/yote/setup.pl ===========

(follow the bouncing ball of actions to do. Pawprint is what
 you do)

On setup these are the choices you must make :
  * path for the yote root directory (default /opt/yote)
  * path for cgi scripts (default /var/www/cgi-bin)
  * document root path for the webserver (default /var/www/html)
  * master admin account name and password
  

This bootstrapping example uses Apache2.

  <pawprint>$ sudo yote_setup
  yote_setup version 0.1 starting
  <pawprint>enter yote root_directory ) /opt/yote
  <pawprint>enter the apache document root ) /var/www/html
  <pawprint>enter the cgi directory ) /var/www/cgi-bin
  <pawprint>enter yote master admin account name ) coyo
  <pawprint>enter yote master admin password ) ******
  <pawprint>repeat yote master admin password ) ******

yote_setup then
   * writes /opt/yote/setup.pl that which
         sets a library path and YOTE_ROOT_DIRECTORY
         to /opt/yote
   * creates a data store at /opt/yote/datastore
   * copies adapter.cgi to /var/www/cgi-bin/

   /opt/yote/setup.pl
      #!/usr/bin/perl
      use strict;
      use lib '/opt/yote/lib';
      $ENV{YOTE_ROOT_DIRECTORY} = '/opt/yote/';
      1;

   /opt/yote/datastore
      contains Data::ObjectStore with the following data :
      
Apache specifics :

<pawprint>make sure apache has modperl and cgi set up.
<pawprint>edit the apache config files to look like these

  httpd.conf ----
    DocumentRoot "/var/www/html"
    <Directory "/var/www">
        AllowOverride None
        Require all granted
    </Directory>
    <Location /app>
         SetHandler perl-script
         PerlHandler Yote::App::ApacheAdapter
    </Location>
    <IfModule alias_module>
        ScriptAlias /cgi-bin/ "/var/www/cgi-bin/"
    </IfModule>

  perl.conf ----
    PerlPostConfigRequire /opt/yote/setup.pl
    PerlConfigRequire /opt/yote/setup.pl
    PerlModule Yote::App::ApacheAdapter
    <Location /app>
        SetHandler perl-script
        PerlResponseHandler Yote::App::ApacheAdapter
        PerlOptions +ParseHeaders
    </Location>

#---------------------------------------------------------

=========== Yote Design ===========

The purpose of Yote is to provide endpoints
and backends for webapps and other type of apps.

An app is composed of the following parts :
  * backend data
  * backend logic
  * ui

== Scripts ==

  yote_setup -

  yote_manage_apps -

--Bootstrapping Steps--

These steps describe how yote is set up and run for the first time

webserver starts
 0) has modperl and cgi adapter endpoints.
   These endpoints handle all incoming yote requests over all apps.
    1)  These endpoints have a library path
    2)  These endpoints have a way to connect to the store(s)

scenarios

 ** single host, mulitple domains, single store, multiple apps **

 ** multiple hosts, mulitple domains, single store, multiple  apps **

 ** multiple hosts, mulitple domains, started stores, multiple apps **


#---------------------------------------------------------



yote_setup
  * writes /etc/yote.conf
  * writes modperl and cgi files

Info in /etc/yote.conf
  - yote directory : default is /opt/yote

/opt/yote contains
  - object store
  - library path
  - app subdirectory
      - html subdirectory (symlinked to /var/www/html/<appname>
      - lib directory (symlinked to library path)

webserver directory contains
  /var/www/cgi/
  /var/www/html/<appname>/

_________________________________________


