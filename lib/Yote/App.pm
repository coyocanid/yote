package Yote::App;

use strict;
use warnings;

no warnings 'numeric';

use Yote::Obj;
use base 'Yote::Obj';

use Digest::MD5;
use File::Path qw(make_path);

sub _init {
    my $self = shift;
    $self->SUPER::_init;
    $self->set__app_accounts({});
    $self->set__session({});
    $self->set__login({});
    $self->set__login2account({});
    $self->set__email({});
} #_init


# ------------------ UTILITY METHODS ----------------

# override to set a new login package
sub _new_account_package {
    return 'Yote::App::Account';
}

sub account {
    my( $self, $login ) = @_;
    my $accts = $self->get__login2account({});
    my $account = $accts->{$login};
    unless( $account ) {
	$account = $accts->{$login} = $self->store->create_container( $self->_new_account_package,
								      {
									  login_name => $login->get_login_name,
									  _login     => $login,
								      } );
    }
    return $account;
} #account

sub create_login {
    my( $self, $args ) = @_;
    my( $login_ok ) = $self->check_login( $args->{login} );
    my( $email_ok ) = $self->check_email( $args->{email} );
    if( $login_ok  && $email_ok ) {
        my( $un, $em, $pw ) = @$args{qw( login email password ) };
        my $enc_pw = $self->crypt_password( $pw );
        my $time = time;

        my $login = $self->store->create_container( 'Yote::App::Login',
                                                    {
							#
							# this is all hidden from the client.
							# an app might override this and add
							# a handle for the user, for instance.
							# 
                                                        _active_time => $time,
                                                        _login_time  => $time,
                                                        _created     => $time,
                                                        login_name   => $un,
                                                        _email       => $em,
                                                        _enc_pw      => $enc_pw,
                                                    } );
        $self->get__email->{lc($em)} = $login;
        $self->get__login->{lc($un)} = $login;

        return $login;
    }
} #create_login


sub find_session {
    my( $self, $sess_id ) = @_;
    
#    $self->lock( 'Yote_App_SESSION' );
    my $sessions = $self->get__session({});
    my $sess = $sess_id ? $sessions->{$sess_id} : undef;
    print STDERR "))$sess_id --> $sess\n";
    if( $sess ) {
#        $self->unlock;
        return $sess;
    }
    
    $sess = pop @{$self->get__session_pool([])};
    if( $sess ) {
        $self->get__session({})->{$sess->session_id} = $sess;
        return $sess;
    }
    $sess = $self->store->create_container( 'Yote::App::Session' );
    $sess->set_app( $self );

    $self->register_session( $sess );
    
    $self->store->save;
#    $self->unlock;
    return $sess;
} #find_session

sub register_session {
    my ($self, $session) = @_;
    $self->get__session({})->{$session->session_id} = $session;
} #register_session

# ------------------ APP PUBLIC METHODS NOT NEEDING LOGIN ----------------

sub check_login {
    my( $self, $login ) = @_;
    if( $self->get__login({})->{lc($login)} ) {
        return 0, 'login already taken';
    } elsif( length($login) < 2 ) {
        return 0, 'login too short';
    } 
    return 1, 'not taken';
} #check_login

sub check_email {
    my( $self, $email ) = @_;
    if( $self->get__email({})->{lc($email)} ) {
        return 0, 'email already taken';
    } elsif( $email !~ /^[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+$/ ) {
        return 0, 'malformed email';
    } else {
        return 1, 'not taken';
    }
} #check_email

sub load {
    my $self = shift;
    return 1, $self;
}

sub has_login {
    my( $self, $args, $sess ) = @_;
    if (my $login = $sess->get_login) {
	return 1, [$self->account($login),$login];
    }
    return 0, [];
}

sub login {
    my( $self, $args, $sess ) = @_;
    
    #    $self->lock( 'Yote_App_LOGIN' );
    if ($sess->get_login) {
	return 0, 'already logged in';
    }
    
    my( $unORem, $pw ) = ( lc( $args->{user} ), $args->{password} );
    my $login;
    if( $unORem =~ /\@/ ) {
        $login = $self->get__email->{$unORem};
    } else {
        $login = $self->get__login->{$unORem};
    }
    if( $login ) {
        my $enc_pw = $login->crypt_password($pw);
        if( $enc_pw eq $login->get__enc_pw ) {
            $sess->set_login( $login );
            $sess->set_app( $self );
            return 1, $self->account($login);
        }
    } else {
        # calculate to prevent a timing oracle.
        $login = crypt( $pw, length( $pw ) . Digest::MD5::md5_hex($unORem) );
    }
    return 0, "login failed";

} #login

sub logout {
    my( $self, $args, $sess ) = @_;
    my $sess_id = $sess->session_id;
    delete $self->get__session->{$sess_id};
    $sess->clear;
    return 1, $self;
} #logout

sub signup {
    my( $self, $args, $sess ) = @_;

    # $args -> { login => 'login', email => 'email', password => 'pw' }

    my $login = $sess && $sess->get_login;
    if( $login ) {
        return 0, 'already have a login';
    }
    
    $self->lock( '_credentials' );

    #    if ( length($args->{password})  < 7 ) {
    if ( length($args->{password})  < 4 ) {
        return 0, "Password too short";
    }
    $login = $self->create_login( $args );
    unless( $login ) {
	my( $login_ok, $login_fail ) = $self->check_login( $args->{login} );
	my( $email_ok, $email_fail ) = $self->check_email( $args->{email} );
        return 0, (!$login_ok && $login_fail ) || ( !$email_ok && $email_fail ) || "signup failed";
    }

    $sess->set_login( $login );
    $sess->set_app( $self );

    return 1, $self->account($login);

} #signup



# ------------------ METHODS NEEDING LOGIN ----------------

1

__END__

recap 
  client ---> yote-endpoint.cgi
                 Yote::Endpoint->load->handle
                     appname -> app
                     app(sess_id) -> session
                     (session || endpoint)->umarshal args
                       
               


sub fetch {
    my( $self, $args, $sess ) = @_;
    $sess->empty_cache;
    return 1, shift;
}

sub note {
    my( $self, $msg, $acct ) = @_;
    my($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
    my $tdis = sprintf( "[%02d/%02d/%02d %02d:%02d]", $year%100,$mon+1,$mday,$hour,$min );
    push @{$self->vol( '_notes' )}, "$tdis $msg".($acct ? " - ".$acct->get_login_name : '' );
} #note


sub _make_reset_link {
    my( $self, $email, $sess ) = @_;
    
    my $login = $self->get__email->{lc($email)};

#    $self->lock( 'Yote_App_RESET_REQ' );
    my $resets = $self->get__resets({});

    my $res_tok;
    my $found;
    my $tries = 10;
    until( $found ) {
        $res_tok  = int( rand( 10_000_000 ) );
        $found = ! $resets->{$res_tok};
    }
    my $gooduntil = time + 3600;

    my $site = $self->get__site;
    my $app_path = $self->get__app_path;    
    my $link = "https://$site/$app_path\#recover;tok=$res_tok";
    
    if( $login ) {
        # nuke old token if any
        if( my $old_tok = $login->get___reset_token ) {
            delete $resets->{$old_tok};
        }
        $resets->{$res_tok} = $login;
        $login->set___reset_token( $res_tok );
        $login->set___reset_token_good_until( $gooduntil );
        $self->note( "RESET REQUEST FOR '$email'" );
        return 1, $link;
    } 
    $self->note( "RESET REQUEST FOR UNKNOWN EMAIL '$email'" );
    return 0, $link;

} #_make_reset_link

sub send_reset_request {
    my( $self, $email ) = @_;

    my( $res_suc, $link ) = $self->_make_reset_link( $email );
    
    my $body_html = $self->get__reset_email_body(<<"END");
Recover your account <a href="LINK">LINK</a>
END
    $body_html =~ s/\bLINK\b/$link/gs;
    my $body_txt = $self->get__reset_email_txt(<<"END");
Recover your account LINK
END
    $body_txt =~ s/\bLINK\b/$link/gs;
    my $subj = $self->get__reset_email_subj("recovery link");
    $subj =~ s/\bLINK\b/$link/gs;
    
    my $site = $self->get__site;
    
    my $msg = MIME::Lite->new(
        From => "noreply\@$site",
        To   => $email,
        Subject => $subj,
        Type => 'multipart/alternative',
        );

    $msg->attach(Type => 'text/plain', Data => $body_txt);
    $msg->attach(Type => 'text/html',
                 Data => $body_html,
                 Encoding => 'quoted-printable');
    
    if( $res_suc ) {
        # prevent a timing oracle. Do everything up until the send
        $msg->send;
    }

    # always say the email was sent
    return 1, "sent";
} #send_reset_request


sub update {
    return 1;
}

# ------------ LOGIN-REQUIRED --------------

sub reset_password {
    my( $self, $args, $sess ) = @_;
    my $login = $sess->get_login;
    my $pw = $args->{newpassword};
#    if( length($pw) < 7 ) {
    if( length($pw) < 4 ) {
        return 0, "Password too short";
    }
#    $self->lock( 'RESET_$login' );
    my $reset_token = $args->{token};
    if( $reset_token ) {
        delete $self->get__resets->{$reset_token};
        if( !$login || $reset_token ne $login->get___reset_token ) {
            return 0, 'bad or expired token';
        }
    }
    elsif( $login ) {
        my $oldpw = $args->{oldpassword};
        my $un = $login->get_login_name;
        my $enc_old_pw = crypt( $oldpw, length( $oldpw ) . Digest::MD5::md5_hex($un) );
        if( $enc_old_pw ne $login->get___enc_pw ) {
            return 0, 'wrong password';
        }
    }
    else {
        return 0, "failed";
    }
    $self->note( "reset password", $login );
    $login->set___reset_token(undef);
    $login->set___reset_token_good_until(undef);
    my $un = $login->get_login_name;
    my $enc_pw = crypt( $pw, length( $pw ) . Digest::MD5::md5_hex($un) );
    $login->set___enc_pw( $enc_pw );
    return 1, "Reset Password";
} #reset_password


# ------------ UTIL --------------


sub init_notes {
    my $self = shift;
    $self->vol( '_notes', [] );
}


sub write_notes {
    my( $self ) = @_;
    my $notes = $self->vol('_notes');
    if( $notes ) {
#    $self->unlock;
#        $self->lock( "Yote_App-".$self->get__app_path."-NOTES" );
        $self->add_to__logs( @$notes );
        $self->clearvol( '_notes' );
    }
} #_write_notes


1;

__END__

=head1 NAME

Yote::App - Base class for app objects

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHODS

The methods are in two flavors : normal and app methods.
The app methods always take an argument and session field.

=head2 calc_config( config, app_path )

Returns the config as a hash, caluclated
from the current state of the app object.

=head2 apply_config( config, app_path )

Sets up the app according to the config
and places it in the app path.
Returns the config as a hash.

=head2 methods_for( cls, sess )

Returns a list ref of method names
that are available for the given class
and session (based on login state of session)

=head2 may_use_method( method, obj, sess )

Returns truthy if the method name given is allowed
to be invoked on the object according to the login
state of the session.

=head2 activate_reset

App method. The argument is a an activation token.
Logs in the login associated with the token
to the given session.

=head2 check_login

App method. The argument is a hash ref with the fields 'login' and 'email'
The args is a hash ref containging login and email. This returns a success if neither of those are
already in use.

=head2 check_email

App method. The argument is a hash ref with the fields 'login' and 'email'
The args is a hash ref containging login and email. This returns a success if neither of those are
already in use.

=head2 fetch 

App method. Simply returns this app object.

=head2 login

App method. The argument is a hash ref with the fields 'user' and 'password'. The 'user' field can be login name or email.
If the user and password match up to a login, that login is attached to the session.

=head2 send_reset_request

App method. The argument is an email address.
Creates a reset token for the login with the
given email. If a login is associated with
the session, it sends a login link to the given
email.

=head2 signup

App method. The argument is a hash ref with the fields 'login', 'email' and 'password'. 
The args is a hash ref containging 'login', 'password' and 'email'. If the login and email are new to the system and the password is acceptable, a new login is created and attached to the session.

=head2 update

An app method that does nothing. Yote automatically updates the state of all client side objects upon any call. That is what this otherwise no-op method is for.

=head2 logout 

App method that clears out the session object and removes it from
the sessions.

=head2 reset_password

App method. The argument is a hash ref with the fields 'newpassword' and 'oldpassword' or 'token'. If oldpassword is given, a login must be attached to the session. If a token is given, it must have been issued by a reset link.


=head2 find_session

=head2 init_notes

=head2 note

=head2 write_notes


Returns the app object under the given path.

=head1 AUTHOR
       Eric Wolf        coyocanid@gmail.com

=head1 COPYRIGHT AND LICENSE

       Copyright (c) 2012 - 2019 Eric Wolf. All rights reserved.  This program is free software; you can redistribute it and/or modify it
       under the same terms as Perl itself.

=head1 VERSION
       Version 0.1  (Aug, 2019))

=cut
v
