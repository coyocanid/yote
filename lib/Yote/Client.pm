package Yote::Client;

use strict;
use warnings;

use Yote::Client::Obj;
use JSON;

sub new {
    my ($cls,$endpoint) = @_;
    return bless {
	endpoint     => $endpoint,
	app2sessions => {},
	app2cache    => {},
	obj2id       => {},
    }, $cls;
} #new

sub muster {
    my $item = shift;
    my $r = ref( $item );
    if( $r ) {
	if ($r eq 'ARRAY') {
	    return [map {muster($_)} @$item];
	} elsif( $r eq 'HASH' ) {
	    return {map {$_ => muster($item->{$_})} keys %$item};
	} 
	return "r$item->{id}";
    } elsif( defined( $item ) ) {
	return "v$item";
    }
    return "u";
} #muster

sub id {
    my ($self, $obj) = @_;
    my $r = ref($obj);
    if ($r && $r !~ /^(HASH|ARRAY)$/ ) {
	return $self->{obj2id}{$obj->{data}};
    }
    return $self->{obj2id}{$obj};
} #id

sub call {
    my ($self, $app, $target, $action, $args) = @_;

    
    my $sess_id = $self->{app2sessions}{$app};
    my $cache   = $self->{app2cache}{$app} //= {};

    my ($contentType, $result) = $self->{endpoint}->handle( { payload => encode_json( {
	action  => $action,
	args    => muster( $args ),
	app     => $app,
	sess_id => $sess_id,
	target  => $target,
										      } ) } );

    my $decoded = decode_json( $result );
    
    my $data = $decoded->{data};
    
    my $localcache = {};
    
    my $unmuster = sub {
	my $item = shift;
	if ($item eq 'u') { return undef; }
	if ($item =~ /^r(.*)/) { return $localcache->{$1}; }
	return substr($item,1);
    };

    for my $key (keys %$data) {
	my $val = $data->{$key};
	my ($cls, $itemData) = @$val;
	my $thing = $localcache->{$key};
	unless ($thing) {
	    if ($cls eq 'ARRAY') {
		# this may allow leakage if eventually a hash key collides.
		# gonna say that's not going to happen, but the cache will fill
		# up with hashes unless we do a tied thingy like Data::ObjectStore::Array
		$thing = $localcache->{$key} = $cache->{$key} //= [];
	    }
	    elsif ($cls eq 'HASH') {
		$thing = $localcache->{$key} = $cache->{$key} //= {};
	    }
	    else {
		my $o =
		    $localcache->{$key} =
		    $cache->{$key} //=
		    Yote::Client::Obj->new( {
			app     => $app,
			class   => $cls,
			client  => $self,
			data    => ref($itemData) eq 'ARRAY' ? [] : {},
			id      => $key,
			methods => $decoded->{defs}{$cls},
					    } );
		$thing = $o->{data};
	    }
	    $self->{obj2id}{$thing} = $key;
	}
    }
    for my $key (keys %$data) {
	my $val = $data->{$key};
	my ($cls, $itemData) = @$val;
	my $thing = $localcache->{$key};
	if ($cls eq 'ARRAY') {
	    @{$thing} = map { $unmuster->($_) } @$itemData;
	    #@{$thing} = @$itemData;
	} elsif ($cls eq 'HASH') {
	    %{$thing} = map { $_ => $unmuster->($itemData->{$_}) } keys %$itemData;
	} else {
	    %{$thing->{data}} = %$itemData;
	}
    }
    $self->{app2sessions}{$app} = $decoded->{sess_id};

    return $decoded->{succ}, $unmuster->( $decoded->{ret} );
    
} #call

sub load {
    my ($self,$app) = @_;
    $self->{app2sessions}{$app} = {};
    $self->{app2cache}{$app} = {};
    return $self->call( $app, undef, 'load' );
} #load

1;
