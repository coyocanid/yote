package Yote::App::Account;

use strict;
use warnings;

use base 'Yote::Obj';

sub _load {
    my $self = shift;
    $self->get_avatars([]);
    $self->fix_methods( qw ( 
			remove_avatar
			use_avatar
			update_bio
			upload_avatars
			) );
}

sub update_bio {
    my ($self, $bio ) = @_;
    return 1, $self->set_bio( $bio );
}


sub use_avatar {
    my ($self, $ava ) = @_;
    return 1, $self->set_avatar( $ava );
}

sub remove_avatar {
    my ($self, $ava ) = @_;
    my $avas = $self->get_avatars;
    my $aid = $self->store->existing_id( $avas );
    print STDERR "REMOVE $ava from ARRAY $aid, existing avatar is ".$self->get_avatar."\n";
    print STDERR "BEFORE REMOVE " . join ( " ", map { "$_" } @$avas );
    my $removed = $self->remove_from_avatars( $ava );
    print STDERR "AFTER REMOVE " . join ( " ", map { "$_" } @$avas );
    if ($removed == $self->get_avatar) {
	my $avas = $self->get_avatars;
	print STDERR "RESETTING AVATAR\n";
	if (@$avas) {
	    $self->set_avatar($avas->[0]);
	}
    }
    print STDERR "AVATAR NOW ".$self->get_avatar."\n";
    return 1, $removed;
}

sub upload_avatars {
    my ($self, $args, $session, $uploader) = @_;
    my $avatars = $self->get_avatars;
    while (my $img = $uploader && $uploader->()) {
	$self->get_avatar( $img ); # sets avatar if none
	push @$avatars, $img;
    }
    return 1, $avatars;
} #upload_avatars

1;
