package Yote::Domain;

# 
# maps apps to a domain key
#
# knows about the yote-conf.yaml file format
#

use strict;
use warnings;

use Yote::App;

use Data::ObjectStore;
use base 'Data::ObjectStore::Container';

sub _init {
    my $self = shift;
    $self->set_apps( {} );
    $self->get_domain;
}

sub calc_config {
    my( $self, $cfg, $dom ) = @_;
    my $dom_cfg = $cfg->{$dom} //= {};
    my $paths = $self->get_apps;
    my $cfg_paths = $dom_cfg->{APPS} //= {};
    my @gone_paths = grep { ! $paths->{$_} } keys %$cfg_paths;
    for my $gone (@gone_paths) {
        delete $cfg_paths->{$gone};
    }
    for my $path (keys %$paths) {
        $paths->{$path}->calc_config( $cfg_paths, $path );
    }
    return $dom_cfg;
} #calc_config

sub apply_config {
    my( $self, $domain_cfg ) = @_;

    my $dom = $self->get_domain;

    my $apps_cfg = $domain_cfg->{APPS};

    my( $paths, $old_paths ) = ( $apps_cfg, $self->get_apps );

    my @new_paths  = grep { ! $old_paths->{$_} } keys %$paths;
    my @gone_paths = grep { ! $paths->{$_} } keys %$old_paths;
    my @same_paths = grep { $paths->{$_} } keys %$old_paths;

    # new paths
    for my $path (@new_paths) {
        $self->install_app( $path, $apps_cfg->{$path} );
    }

    # removed paths
    for my $path (@gone_paths) {
        $self->remove_app( $path );
    }
    
    warn "There are no apps defined for domain '$dom'" unless @new_paths || @same_paths;
    
    # same paths
    for my $path (@same_paths) {
        my $app_cfg = $apps_cfg->{$path};
        my $app = $self->get_apps->{$path};
        if( ref( $app ) eq $app_cfg->{'MAIN-APP-CLASS'} ) {
            $app->apply_config( $app_cfg );
        }
        else {
            $app = $self->install_app( $path, $app_cfg );
        }
    }
} #apply_config

sub install_app {
    my( $self, $path, $app_cfg ) = @_;
    my $dom = $self->get_domain;
    my $cls = $app_cfg->{'MAIN-APP-CLASS'};
    eval "use $cls";
    if( ! $cls->isa( "Yote::App" ) ) {
        die "Domain '$dom' was not able to load app '$path' of class '$cls' because it is not a Yote::App descendent";
    }
    else {
        my $old_app = $self->get_apps->{$path};
        if( $old_app ) {
            warn "replacing domain '$dom' app '$path' which was class '".ref($old_app)."' with app of class '$cls'";
            $self->remove_app( $path );
        }
        my $app = $self->store->create_container( $cls );
        $app->set__site( $dom );
        $app->set__domain( $self );
        $app->apply_config( $app_cfg );
        $app->set__app_path( $path );
        $self->get_apps->{$path} = $app;
        return $app;
    }
} #install_app

sub remove_app {
    my( $self, $app_path ) = @_;
    my $dom = $self->get_domain;
    my $app = delete $self->get_apps->{$app_path};
    warn "Removing app '$app_path' from domain '$dom' (but archiving it to /yote/domains/$dom/_deleted_apps)";
    $self->add_to__deleted_apps( $app );
} #remove_app

sub fetch_app {
    my( $self, $app_path ) = @_;
    return $self->get_apps->{$app_path};
}


1;

__END__


=head1 NAME

Yote::Domain - Domain object for Yote

=head1 SYNOPSIS


=head1 DESCRIPTION

=head1 METHODS

=head2 load( yote_root_dir )

Opens up the yote root directory and loads up the
yote-conf.yaml config file.

=head2 calc_config

Calculates the config based on the current object state and
returns it as a hash.

=head2 apply_config( domain_config )

Applies the new config for this domain. This installs
and/or removes apps.

=head2 install_app( path, app_config )

Installs the app to this domain under the given path
and by the given app config.

=head2 remove_app( path )

Removes the app from the given path and archives it under
the _deleted_apps field.

=head2 fetch_app( path )

Returns the app object under the given path.

=head1 AUTHOR
       Eric Wolf        coyocanid@gmail.com

=head1 COPYRIGHT AND LICENSE

       Copyright (c) 2012 - 2019 Eric Wolf. All rights reserved.  This program is free software; you can redistribute it and/or modify it
       under the same terms as Perl itself.

=head1 VERSION
       Version 0.1  (Aug, 2019))

=cut
