package Yote::Obj;

#
# An alias for Data::ObjectStore::Container.
# Doing this because the package name 'Yote::Obj'
# clearly belongs to Yote
#

use strict;
use warnings;

use Data::ObjectStore;
use base 'Data::ObjectStore::Container';

sub _init {
    my $self = shift;
    $self->SUPER::_init;
    $self->set__allowed_methods({});
} #_init

sub allowed_methods {
    my $self = shift;
    my $allowed = $self->get__allowed_methods({});
    return [grep { $allowed->{$_} } keys %$allowed];
}

sub is_allowed_method {
    my ($self, $method ) = @_;
    return $self->get__allowed_methods({})->{$method};
}

sub allow_methods {
    my ($self, @methods ) = @_;
    for my $method (@methods) {
	$self->get__allowed_methods({})->{$method} = 1;
    }
}
sub fix_methods {
    my ($self, @methods ) = @_;
    my $allowed = $self->get__allowed_methods({});
    %$allowed = ( map { $_ => 1 } @methods );
}
sub disallow_methods {
    my ($self, @methods ) = @_;
    for my $method (@methods) {
	delete $self->get__allowed_methods({})->{$method};
    }
}


1;
