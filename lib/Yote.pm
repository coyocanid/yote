package Yote;

use strict;
use warnings;

use Data::Dumper;

use Data::ObjectStore;
use base 'Data::ObjectStore::Container';

use vars qw($VERSION);
$VERSION = '0.1';

our $SINGLETON;

sub activate {
    my ($pkg, $yote_config) = @_;

    if ($SINGLETON) { return $SINGLETON; }
    my $store = Data::ObjectStore->activate_store( directory => $yote_config->{record_store},
						   memory_permissions => 0777 );
    unless( $store ) {
	return undef;
    }
    my $root = $store->fetch_root;
    my $yote = $root->get_yote;

    unless( $yote ) {
	$yote = $root->set_yote( $store->create_container( 'Yote', { _config => {} } ) );
    }
    my $config = $yote->get__config;
    %$config = %$yote_config;
    
    my $apps          = $yote->get_apps;
    my $active_apps   = $yote->get_active_apps;
    my $archived_apps = $yote->get_archived_apps;

    # install new apps
    for my $appname (keys %{$yote_config->{apps}}) {
	my $appdef = $yote_config->{apps}{$appname};
	my $appclass = $appdef->{package};

	my $app = $apps->{$appname};
	my $oldref = ref($app);
	if ( $oldref && $oldref ne $appclass ) {
	    warn "App '$appname' was class '$oldref' but is being reinstalled as '$appclass'. The old app is placed in the archived_apps list";
	    push @$archived_apps, $app;
	    $app = undef;
	}
	unless ($app) {
	    print STDERR "yote adding '$appname' of class '$appclass'\n";
	    $app = $apps->{$appname} = $store->create_container( $appclass, { _app_path => $appname, _yote => $yote } );	    
	}

	# ensure admin logins
	for my $login_skel (@{$appdef->{admin}||[]}) {
	    my $login = $app->get__login->{$login_skel->{login}};
	    if ($login) {
		$login->set_is_admin(1);
	    } elsif( my $login = $app->create_login( $login_skel )) {
		$login->set_is_admin(1);
		$login->set_needs_password_reset(1);
	    } else {
		warn "Unable to create admin login for $login_skel->{login}";
	    }
	}
	

    } #install new apps

    # archive apps not in the install list
    for my $appname (keys %$apps) {
	unless ( $yote_config->{apps}{$appname} ) {
	    warn "App '$appname' of class '$appname' is no longer marked as installed. Placing in the archived_apps list";
	    my $app = delete $yote_config->{apps}{$appname};
	    push @$archived_apps, $app;
	}
    }

    # activate apps
    my %old_active = %$active_apps;
    %$active_apps = ();
    for my $appname (@{$yote_config->{active_apps}}) {
	$active_apps->{$appname} = $apps->{$appname};
	unless ($old_active{$appname}) {
	    print STDERR "yote activating '$appname' of class '".ref($apps->{$appname})."'\n";
	}
    }

    $store->save;
    print STDERR "SAVING\n";
    $SINGLETON = $yote;
    return $yote;
    
} #activate

sub load {
    my ($pkg, $yote_config) = @_;

    if ($SINGLETON) { return $SINGLETON; }

    my $store = Data::ObjectStore->open_store( $yote_config->{record_store} );
    unless( $store ) {
	print STDERR Data::Dumper->Dump([$@,$!,"NOSTO"]);
	return undef;
    }
    $store->fetch_root;
    if ($store) {
	$SINGLETON = $store->fetch_root->get_yote;
    }
    return $SINGLETON;
} #load

sub close {
    my ($self) = @_;
    $self->store->close_objectstore;
}

sub fetch_app {
    my ($self, $appname) = @_;
    return $self->get_active_apps->{$appname};
}

sub _init {
    my $self = shift;
    $self->SUPER::_init;
    $self->set_apps({});
    $self->set_active_apps({});
    $self->set_archived_apps([]);
}


1;

__END__

=head1 NAME

Yote - OOP APP platform

=head1 SYNOPSIS


=head1 DESCRIPTION



=head1 VERSION
       Version 0.1  (Aug, 2019))

=cut
