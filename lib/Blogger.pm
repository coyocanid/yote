package Blogger;

use strict;
use warnings;

use Blogger::Account;
use Blogger::Blog;

use base 'Yote::App';



sub _load {
    my $self = shift;
    $self->SUPER::_load;
    $self->fix_methods( qw ( 
			check_login
			check_email
			login
			logout
			has_login
			load
			signup

			) );
    $self->get_bloggers( {} );
    $self->get_recent( [] );
    $self->get_main_blog();
}


sub _new_account_package {
    return 'Blogger::Account';
}


1;
