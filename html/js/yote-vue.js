
import "/js/vue.js";
import { yote } from '/js/yote/yote.js';

const $ = (sel,el) => (el||document).querySelector( sel );
const $$ = (sel,el) => Array.from((el||document).querySelector( sel ));

const root = $('#yote-root');
const appname = root.dataset.app;

Vue.component( 'BootstrapLogin', {
    props    : [ 'app' ],
    data     : function() { return { user : '', password : '', error : '' } },
    methods  : {
	login : function() {
	    this.app.login( { user     : this.user,
			      password : this.password } )
		.then(
		    account => {
			this.password = '';
			this.user     = '';
			this.$emit( 'login', account );
		    },
		    err => this.error = err,
		);
	},
    },
    template : `
<div class="login">
  <div>{{ error }}</div>
  <form class="col"
        v-on:submit.prevent="login">
    <input type="text"
	   v-model="user" placeholder="account or email">
    <input type="password" v-model="password" placeholder="password">
    <button type="submit">Log In</button>
  </form>
</div>`,
} ); //bootstrap-login


// define editor component
Vue.component( 'AppEditor', {
    props : [ 'app', 'login' ],
    methods : {
	logout : function() {
	    this.app.logout().then( this.appLogin = undefined );
	},
    },
    data : function() {
	return {
	    appLogin : this.login,
	}
    },
    template : `
<div v-if="appLogin && appLogin.needs_password_reset">
  <h1>You are admin and need to reset your password</h1>
</div>
<div v-else-if="appLogin && appLogin.is_admin">
  <h1>You are admin and are editing app <b><i>${appname}</i></b></h1>
  <a href="/logout" v-on:click.prevent="logout">Log Out</a>
</div>
<div v-else-if="! app.is_live">
  <h1>Login as admin to start building app <b><i>${appname}</i></b></h1>
  <bootstrap-login v-bind:app="app"
                   v-on:login="appLogin = $event"
		   ></bootstrap-login>
</div>
<div v-else>
  <h1>App Is Live</h1>
</div>
`,
} );

yote.fetchAppAndAccount( appname, { endpoint : '/cgi-bin/yote-endpoint.cgi' } )
    .then( (triplet) => {
	const [app,account,login] = triplet;
	new Vue( {
	    el : '#yote-root',
	    data : {
		account,
		app,
		login,
	    },
	    template : app.is_live ? `
<div>
 <h1>The App Is Live</h1>
</div>
` : `
<div>
  <app-editor v-bind:app="app" v-bind:login="login"></app-editor>
</div>
`,
	} );
    } );
