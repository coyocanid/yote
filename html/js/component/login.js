import { e, render, Component } from '/js/backdraft/lib.js';

class Login  extends Component {
    constructor( kwargs ) {
	super( kwargs );
	this.app = kwargs.app;
    }
    bdElements() {
	return e.div(
	    e.div( {
		class     : 'for-logged-in',
		bdReflect : [ this.app, '_login', login => `Hello ${login.get('login')}` ],
	    } ),
	    e.div( ),
	);
    }
}

export { Login }
