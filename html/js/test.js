import { yote } from "/js/yote/yote.js";

let tests  = 0;
let passes = 0;
let fails  = 0;

const is = (a,b,msg) => {
    tests++;
    if( a === b ) {
	passes++;
	console.log( `passed : ${msg}` );
    } else {
	fails++;
	console.log( `failed : ${msg} '${a}' differs from '${b}'` );
    }
}

const isnt = (a,b,msg) => {
    tests++;
    if( a !== b ) {
	passes++;
	console.log( `passed : ${msg}` );
    } else {
	fails++;
	console.log( `failed : ${msg} '${a}' differs from '${b}'` );
    }
}

const done_testing = () => {
    if ( fails > 0 ) {
	console.log( `failed ${fails} of ${tests} tests` );
    } else {
	console.log( `passed all ${tests} tests` );
    }
}

async function test() {
    let app = await yote.fetchYoteApp( 'test' );

    let msg;
    msg = await app.reset();
    // naw, reset fully resets yote, so the app is gonna be new
    isnt (msg,app,'reset message is app');
    app = await yote.fetchYoteApp( 'test' );
    
    msg = app.get('message');
    is (msg, undefined, 'no message yet');
    
    msg = await app.install_message('fee fi fo fum' );
    is (msg,'fee fi fo fum', 'installed message' );
    is (app.get('message'),'fee fi fo fum', 'installed message to app' );

    is (app.cant_call_me, undefined, 'cant call me not attached' );

    let error;
    let resp = await app.install_special_message('special').catch(
	err => error = err
    );
    is (resp, 'need an account to set the message', 'response for install special without login' );
    is (error, 'need an account to set the message', 'error message for install special without login' );

    is (app.get( 'special_message'), undefined, 'test_app special message not set because not logged in' );

    const email = 'me@you.com';
    const login = 'groopu';
    const password = 'passuwuudo';

    msg = await app.check_email( email );
    is (msg,'not taken', 'email not taken');

    msg = await app.check_login( login );
    is (msg,'not taken', 'login not taken');

    resp = await app.logout();
    is (resp.get('message'), 'fee fi fo fum', 'logout gave app' );

    msg = await app.check_email( "BLBLBLBL" ).catch(
	err => error = err
    );
    is (error, 'malformed email', 'email malformed' );

    resp = await app.signup( { email, login, password } );
    let acct = resp;
    is ( acct._app, 'test', 'got object with test app' );
    is ( acct.get('login_name'), 'groopu', 'login name' );

    msg = await app.check_login( login ).catch(
	err => error = err
    );
    is (error,'login already taken', 'login now taken');

    msg = await app.check_email( email ).catch(
	err => error = err
    );
    is (error,'email already taken', 'email now taken');

    msg = await app.install_special_message('can you hear');
    is (msg, 'can you hear', 'message results');

    is (app.get('special_message'), 'can you hear', 'app has special message' );

    msg = await app.login( { user : email, password } ).catch(
	err => error = err
    );
    is (error,'already logged in', 'already logged in message');

    msg = await app.login( { user : `${email}NO`, password } ).catch(
	err => error = err
    );
    is (error,'already logged in', 'already logged in message');

    resp = await app.logout();
    is (resp,app,'logout returns app');

    msg = await app.login( { user : `${email}NO`, password } ).catch(
	err => error = err
    );
    is (error,'login failed', 'login fail message');

    msg = await app.login( { user : email, password : `${password}NO` } ).catch(
	err => error = err
    );
    is (error,'login failed', 'login fail message');

    msg = await app.produce().catch(
	err => error = err
    );
    is (error,'must be logged in to produce', 'produce fail message');
    
    resp = await app.login( { user : email, password } );
    acct = resp;
    is (resp.get('login_name'), 'groopu', 'login data' );

    const o1 = await app.produce();
    is (o1.get('stamp_count'), undefined, 'no count yet' );

    resp = await app.stamp( o1 );
    is (resp, o1, 'stamp returned stamped object');
    is (o1.get('count'), '1', 'count started in obj' );
    is (acct.get('stamp_count'), '1', 'count started in login obj' );

    const o2 = await app.produce();
    is (o2.get('stamp_count'), undefined, 'no count yet for other object' );
    
    resp = await app.stamp( o1 );
    is (resp,o1,'stamp again');
    is (o1.get('count'), '2', 'count again in obj' );
    is (acct.get('stamp_count'), '2', 'count continued in login obj' );
    const info = o1.get('info');
    const info_arry = o1.get('info_arry');
    is (info.got_a_stamp, '2', 'stamp count again in info' );
    is (info_arry[0], '2', 'stamp count again in info array' );
    is (info_arry.length, 1, 'info array has one thing' );

    resp = await app.show_arry( [1,2,3,4] );
    is (resp.length, 1, 'return show arr 1 item' );
    is (resp[0], '4', 'show arry return 4' );
    
    resp = await app.show_arry( [5,6,7,8,9] );
    is (resp.length, 1, 'return still show arr 1 item' );
    is (resp[0], '9', 'show arry return 9' );
    
    console.log( acct,'acct' );
    done_testing();
}

test();
