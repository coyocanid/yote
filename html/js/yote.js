const yoteConfig = {
    endpoint : "/yote",
};
//let endpoint = "cgi-bin/yote-endpoint.cgi",
//let endpoint = "/yote";

let sess_ids_txt  = localStorage.getItem( 'sess_ids' );
let sess_ids = sess_ids_txt ? JSON.parse( sess_ids_txt ) : {};

let cache = {};
let defs  = {};

const marshal = args => {
    if( typeof args === 'object' ) {
        if( args._id ) {
            return "r" + args._id;
        }
        if( Array.isArray( args ) ) {
            return args.map( item => marshal( item ) );
        }
        let r = {};
        Object.keys( args ).forEach( k => r[k] = marshal( args[k] ) );
        return r;
    }
    if (args === undefined )
	return 'u';
    return "v" + args;
} //marshal

const unmarshal = (resp,app) => {
    if( typeof resp === 'object' ) {
        if( Array.isArray( resp ) ) {
            return resp.map( item => unmarshal( item, app ) );
        }
        let r = {};
        Object.keys( resp ).forEach( k => r[k] = unmarshal( resp[k], app ) );
        return r;
    }
    if ( resp === undefined ) { return undefined; }
    var type = resp.substring(0,1);
    var val = resp.substring(1);
    if( type === 'r' ) {
        return cache[app][val];
    }
    else if( type === 'v' ) {
        return val;
    }
    return undefined;
} //unmarshal

const rpc = (app,action,target,args,files,isLoad) => {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.responseType = 'json';
        xhr.open( 'POST', yoteConfig.endpoint );
        xhr.onload = () => {
            if( xhr.status === 200 ) {

		console.log( xhr.response, 'resp' );
		
                // xhr response succ, data, error, ret
                let retv = xhr.response.ret;
		let data = xhr.response.data;
		
                if( sess_ids[app] !== xhr.response.sess_id ) {
                    //clear cache if new session
		    const oldCache = cache[app] || {};
		    Object.keys( oldCache )
		    	.filter( k => ! (k in data) )
			.forEach( k => delete oldCache[k] );
                    sess_ids[app] = xhr.response.sess_id;
                    localStorage.setItem( 'sess_ids', JSON.stringify(sess_ids) );    
                }
                cache[app] = cache[app] || {};

                let inDefs = xhr.response.defs;
                inDefs && Object.keys( inDefs ).forEach( k => defs[k] = inDefs[k] );

		// first round define
                Object.keys( data ).forEach( id => {
                    if( ! cache[app][id] ) {
			const cls = data[id][0];
			const objdata = data[id][1];
			if (cls === 'ARRAY') {
			    cache[app][id] = [];
			} else if (cls === 'HASH') {
			    cache[app][id] = {};
			} else {
			    cache[app][id] = new YoteObj( app, id, objdata, defs[cls] );
			}
                    }
                } );

		// second round update
                Object.keys( data ).forEach( id => {
		    const cls     = data[id][0];
		    const newdata = data[id][1];

		    const item = cache[app][id];
		    if (cls === 'ARRAY') {
			item.splice( 0, item.length, ...newdata.map( item => unmarshal(item,app) ) );
		    } else if (cls === 'HASH') {
			Object.keys( item ).forEach( k => delete item[k] );
			Object.keys( newdata ).forEach( k => item[k] = unmarshal(newdata[k],app) );
		    } else {
                        item._update( newdata );
		    }
                } );
		
                let payload = unmarshal( retv, app );
		xhr.response.succ ? resolve(payload) : reject(payload);

            } else {
                reject('unknown');
            }
        };
        xhr.onerror = () => reject(xhr.statusText);
        
        let fd = new FormData();

        args = marshal( args );
        
        let payload = {
            app,target,action,args,sess_id : sess_ids[app],
        };

	console.log( payload, 'PAY' );
        fd.append( 'payload', JSON.stringify(payload) );
        if( files ) {
            fd.append( 'files', files.length );
            for( let i=0; i<files.length; i++ )
                fd.append( 'file_' + i, files[i] );
        }
        xhr.send(fd);
    } );
}; //rpcs


class YoteObj {
    constructor( app, id, data, def ) {
        this._id = id;
        this._app = app;
        this._data = {};
	this._methods = {}; // adding this field for Vue, to be able to bind right to Vue
        if( def ) 
            def.forEach( mthd => this._methods[mthd] = this[mthd] = this._callMethod.bind( this, mthd ) );
        this._update( data );
    } //constructor

    // get any of the data. The data may not be set, but only updated by server calls
//    get( key ) {
//        return unmarshal( this._data[key], this._app );
//    }
    
    _callMethod( mthd, args, files ) {
        return rpc( this._app, mthd, this._id, args, files );
    }
    _update( newdata ) {
	let updated = false;
        Object.keys( this._data )
            .filter( k => ! k in newdata )
            .forEach( k => {
		delete this[ k ];
		delete this._data[k];
		updated = true;
	    } );
        Object.keys( newdata )
            .forEach( k => {
		if (typeof this[k] === 'function') {
		    console.warn( `Obj ${this._id} clash between method and field for '${k}'` );
		}
		updated = updated || ( this._data[k] === newdata[k] );
		this._data[k] = newdata[k];
		this[k] = unmarshal( this._data[k], this._app );
	    } );
    } //_update

} //YoteObj

const fetchApp = (appName,yoteArgs) => {
    yoteArgs && Object.keys( yoteArgs ).
	forEach( k => yoteConfig[k] = yoteArgs[k] );
    return rpc(appName,'load',undefined,undefined,undefined,true);
};

const fetchAppAndAccount = (appName,yoteArgs) => {
    return new Promise( (res,rej) => {
	fetchApp(appName,yoteArgs)
	    .then(
		app => {
		    if (app.has_login) {
			app.has_login()
			    .then(
				accountAndLogin => {
				    res( [app, ...accountAndLogin] );
				},
				fail    => { res( [app, undefined] ) },
			    );
		    } else {
			res( [app, undefined] );
		    }
		},
		err => rej( err ),
	    );
    } );
}; //fetchAppAndAccoun

class LocationPath {
    constructor(path) {
	this.path = path || [];
	this.top = this.path[0];
    }
    navigate( url, noPush ) {
	console.log( `nav to locationpath ${url}` );
	this.updateToUrl( url );
	if (url != this.url ) {
	    noPush || window.history.pushState( { app : 'test' }, '', url );
	    this.url = url;
	}
    }
    updateToUrl( url ) {
	console.log(`location to ${url}`);
	const matches = url.match( /^(https?:..[^/]+)?([^?#]+)[#?]?(.*)/ );
	const newpath = matches ?  matches[2].split(/\//).filter( p => p.length > 0 ) : [];
	console.log( newpath, matches[2], "NEWP" );
	newpath.shift();
	this.path.splice( 0, this.path.length, ...newpath );
	this.top = this.path[0];
	console.log( this.path.join(" "), this.top, "NEWPATH" );
    }
    subPath() {
	return new LocationPath( this.path.splice(1) );
    }
} //LocationPath

let locationPath; //singleton
const getLocation = () => {
    if (locationPath) return locationPath;
    locationPath = new LocationPath();
    locationPath.navigate( window.location.href );
    window.addEventListener( 'popstate', e => locationPath.navigate( e.target.location.href, true ) );
    return locationPath;
};

const yote = {
    fetchApp,
    fetchAppAndAccount,
    getLocation,
}

export { yote };

/*
const yoteConfig = {
    endpoint : "/yote",
};
//let endpoint = "cgi-bin/yote-endpoint.cgi",
//let endpoint = "/yote";

let sess_ids_txt  = localStorage.getItem( 'sess_ids' );
let sess_ids = sess_ids_txt ? JSON.parse( sess_ids_txt ) : {};

let cache = {};
let defs  = {};

const marshal = args => {
    if( typeof args === 'object' ) {
        if( args._id ) {
            return "r" + args._id;
        }
        if( Array.isArray( args ) ) {
            return args.map( item => marshal( item ) );
        }
        let r = {};
        Object.keys( args ).forEach( k => r[k] = marshal( args[k] ) );
        return r;
    }
    if (args === undefined )
	return 'u';
    return "v" + args;
} //marshal

const unmarshal = (resp,app) => {
    if( typeof resp === 'object' ) {
        if( Array.isArray( resp ) ) {
            return resp.map( item => unmarshal( item, app ) );
        }
        let r = {};
        Object.keys( resp ).forEach( k => r[k] = unmarshal( resp[k], app ) );
        return r;
    }
    if ( resp === undefined ) { return undefined; }
    var type = resp.substring(0,1);
    var val = resp.substring(1);
    if( type === 'r' ) {
        return cache[app][val];
    }
    else if( type === 'v' ) {
        return val;
    }
    return undefined;
} //unmarshal

const rpc = (app,action,target,args,files,isLoad) => {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.responseType = 'json';
        xhr.open( 'POST', yoteConfig.endpoint );
        xhr.onload = () => {
            if( xhr.status === 200 ) {

		console.log( xhr.response, 'resp' );
		
                // xhr response succ, data, error, ret
                let retv = xhr.response.ret;
		let data = xhr.response.data;
		
                if( sess_ids[app] !== xhr.response.sess_id ) {
                    //clear cache if new session
		    const oldCache = cache[app] || {};
		    Object.keys( oldCache )
		    	.filter( k => ! (k in data) )
			.forEach( k => delete oldCache[k] );
                    sess_ids[app] = xhr.response.sess_id;
                    localStorage.setItem( 'sess_ids', JSON.stringify(sess_ids) );    
                }
                cache[app] = cache[app] || {};

                let inDefs = xhr.response.defs;
                inDefs && Object.keys( inDefs ).forEach( k => defs[k] = inDefs[k] );

		// first round define
                Object.keys( data ).forEach( id => {
                    if( ! cache[app][id] ) {
			const cls = data[id][0];
			const objdata = data[id][1];
			if (cls === 'ARRAY') {
			    cache[app][id] = [];
			} else if (cls === 'HASH') {
			    cache[app][id] = {};
			} else {
			    cache[app][id] = new YoteObj( app, id, objdata, defs[cls] );
			}
                    }
                } );

		// second round update
                Object.keys( data ).forEach( id => {
		    const cls     = data[id][0];
		    const newdata = data[id][1];

		    const item = cache[app][id];
		    if (cls === 'ARRAY') {
			item.splice( 0, item.length, ...newdata.map( item => unmarshal(item) ) );
		    } else if (cls === 'HASH') {
			Object.keys( item ).forEach( k => delete item[k] );
			Object.keys( newdata ).forEach( k => item[k] = unmarshal(newdata[k]) );
		    } else {
                        item._update( newdata );
		    }
                } );
		
                let payload = unmarshal( retv, app );
		if (isLoad && payload) {
		    payload.set_login = (login) => {
			payload._login = login;
		    }
		}
		xhr.response.succ ? resolve(payload) : reject(payload);

            } else {
                reject('unknown');
            }
        };
        xhr.onerror = () => reject(xhr.statusText);
        
        let fd = new FormData();

        args = marshal( args );
        
        let payload = {
            app,target,action,args,sess_id : sess_ids[app],
        };

	console.log( payload, 'PAY' );
        fd.append( 'payload', JSON.stringify(payload) );
        if( files ) {
            fd.append( 'files', files.length );
            for( let i=0; i<files.length; i++ )
                fd.append( 'file_' + i, files[i] );
        }
        xhr.send(fd);
    } );
}; //rpcs


class YoteObj {
    constructor( app, id, data, def ) {
        this._id = id;
        this._app = app;
        this._data = {};
	this._methods = {}; // adding this field for Vue, to be able to bind right to Vue
	this._updateHandlers = {};
        if( def ) 
            def.forEach( mthd => this._methods[mthd] = this[ mthd ] = this._callMethod.bind( this, mthd ) );
        this._update( data );
    } //constructor

    // get any of the data. The data may not be set, but only updated by server calls
    get( key ) {
        return unmarshal( this._data[key], this._app );
    }
    
    _callMethod( mthd, args, files ) {
        return rpc( this._app, mthd, this._id, args, files );
    }
    _update( newdata ) {
	let updated = false;
        Object.keys( this._data )
            .filter( k => ! k in newdata )
            .forEach( k => {
		delete this[ 'get_' + k ];
		delete this._data[k];
		updated = true;
	    } );
        Object.keys( newdata )
            .forEach( k => {
		if (!this[ 'get_' + k ]) {
		    this[ 'get_' + k ] = this.get.bind( this, k );
		}
		updated = updated || ( this._data[k] === newdata[k] );
		this._data[k] = newdata[k];
	    } );
	if (updated) {
	    Object.values(this._updateHandlers).forEach( h => h() );
	}
    } //_update

    data() {
	const data = {};
	Object.keys( this._data ).forEach( k => data[k] = this.get(k) );
	return data;
    }

    addUpdateHandler( target, handler ) {
	this._updateHandlers[target] = handler;
    }

    removeUpdateHandler( target ) {
	delete this._updateHandlers[target];
    }
    
} //YoteObj

const fetchYoteApp = (appName,yoteArgs) => {
    yoteArgs && Object.keys( yoteArgs ).
	forEach( k => yoteConfig[k] = yoteArgs[k] );
    return rpc(appName,'load',undefined,undefined,undefined,true);
};

const yote = {
    fetchYoteApp,
}

export { yote, YoteObj };
*/
//rpc('app','action','target',{ name : 'fred' } );
