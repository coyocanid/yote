Vue.component('err', {
    data : function() {
        return { err : this.$attrs.err };
    },
    template : `<div v-if="err" class="err">{{err}}</div>`,
} );

Vue.component('msg', {
    data : function() {
        return { msg : this.$attrs.msg };
    },
    template : `<div v-if="msg" class="msg">{{msg}}</div>`,
} );

Vue.component('login', {
    data : function() {
        return { io : this.$attrs.io };
    },
    template : `<div class="col">
                  <slot></slot>
                  <msg v-bind:msg="io.msg"></msg>
                  <err v-bind:err="io.err"></err>
                  <div class="row">
                    Login <input placeholder="username or email"
                                 v-on:input=""
                                 type="text">
                  </div>
                  <div class="row">
                    Password <input type="password">
                  </div>
                </div>`,
    
} );
