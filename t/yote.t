#!/usr/bin/perl
use strict;
use warnings;

use Yote::Endpoint;
use Yote::Client;

use File::Temp qw/ :mktemp tempdir /;
use IPC::SharedMem;
use IPC::SysV qw(IPC_CREAT IPC_EXCL S_IRWXU memread ftok);
use JSON;
use Test::More;

my( $endpoint, $dir, $yote, $config );
my $sess_cache = {};
 
$dir = prep_dir();
$config = {
    record_store => $dir,
    apps => { test => 'Yote::Test' },
    active_apps => ['test'],
};
$endpoint = Yote::Endpoint->load( $config );
is ($endpoint, undef, 'load failed because Yote was not activated' );

# activate in the init.pl when webserver starts up
$yote = Yote->activate( $config );
my $test_app = $yote->get_active_apps->{test};

#
# $app now is attached to the store loaded by Yote, but if Yote is
# reloaded, then that is a new store thus a new app.
# have to decide what to do with that.
#

ok ( $test_app, 'test app installed' );


my ($succ, $ret);

# -------------------------------------------------------------
#  test basic stuff

# now this can be loaded in the cgi or
#
$endpoint = Yote::Endpoint->load( $config );

my $res = $endpoint->unmarshal( { foo => [ 'vBar', 'u', 'vBaz', {} ], zip => "vzap" } );
is_deeply( $res, { foo => [ 'Bar', undef, 'Baz', {} ], zip => 'zap' }, 'unmsrashl' );

my($contentType, $result) = $endpoint->handle( {} );
like( $result, qr/vfailed, no payload/, 'needs payload' );


my $client = Yote::Client->new( $endpoint );

my $app = $client->load( 'test' );
is (ref $app, 'Yote::Client::Obj', 'client loaded as obj' );
is ($app->{app}, 'test', 'test app loaded' );
my $sess_id = $client->{app2sessions}{test};
like ($sess_id, qr/^S_\d+$/, 'session id' );

is ($app->get_message, undef, 'empty message from test' );

($succ, $ret) = $app->install_message( 'fee fi fo fum' );
is ($succ, 1, 'called install_message on test' );
is ($ret, 'fee fi fo fum', 'app message set result' );
is ($app->get_message,'fee fi fo fum', 'app message set to app object' );
    
($succ, $ret) = $app->cant_call_me;
is ($succ, 0, 'not able to call method' );
is ($ret, "failed : 'cant_call_me' not allowed by 'test'", 'fail message for cant call' );

($succ, $ret) = $app->install_special_message( 'can u beer' );
is ($succ, 0, 'not able to call method needing login' );
is ($ret, 'need an account to set the message', 'fail message for no account try install special message' );
is ($test_app->get_special_message, undef, 'test_app special message not set because not logged in' );

is ($test_app->get_message, 'fee fi fo fum', 'app message set' );
ok ($sess_id, 'get sess_id' );

# -------------------------------------------------------------
#  test accounts
#
my $email = 'me@you.com';
my $login = 'groopu';
my $password = 'passuwuudo';

my $old_sess_id = $app->{client}{app2sessions}{test};
						   
($succ, $ret) = $app->check_email( $email );
is ($succ, 1, 'check login and email no match' );
is ($ret, 'not taken', 'email not taken' );
my $new_sess_id = $app->{client}{app2sessions}{test};

($succ, $ret) = $app->check_login( $login );
is ($succ, 1, 'check login' );
is ($ret, 'not taken', 'login not taken' );

is ($new_sess_id, $old_sess_id, 'sess id didnt change' );

($succ, $ret) = $app->logout();
is ($succ, 1, 'logout worked even if not logged in' );
is_deeply ($ret->{data}, {
	       message => 'vfee fi fo fum',
	   }, 'logout data');

my $newy_sess_id = $app->{client}{app2sessions}{test};
ok( $newy_sess_id ne $old_sess_id, 'new session id after logout' );

($succ, $ret) = $app->signup( { email => $email,
				login => $login,
				password => $password,
			      } );
is ($succ, 1, 'account created' );
is (ref( $ret ), 'Yote::Client::Obj', 'login object' );
is ($ret->{app}, 'test', 'login of test app' );
is ($ret->{class}, 'Yote::App::Login', 'login of test app' );
is_deeply ($ret->{data}, { login_name => 'vgroopu' }, 'no data in logins' );
is_deeply ($ret->{methods}, {}, 'no login methods' );

($succ, $ret) = $app->check_login( $login );
is ($succ, 0, 'check login after sign' );
is ($ret, 'login already taken', 'login is taken' );

($succ, $ret) = $app->check_email( $email );
is ($succ, 0, 'check email after sign' );
is ($ret, 'email already taken', 'email is taken' );

($succ, $ret) = $app->check_email( "BLBLBLBL" );
is ($succ, 0, 'check email ' );
is ($ret, 'malformed email', 'email malformed' );

ok ($new_sess_id, 'got session id returned');
is ($new_sess_id, $sess_id, 'same session id returned');

($succ, $ret) = $app->install_special_message('can you hear');
is ($succ, 1, 'special message worked' );
is ($ret, 'can you hear', 'message results');

is ($test_app->get_special_message, 'can you hear', 'test_app special message set' );

($succ, $ret) = $app->login( { user => "${email}NO",
			       password => $password,
			     } );
is ($succ, 0, 'login failed when already logged in' );
is ($ret, 'already logged in' , 'already logged in  message' );


$old_sess_id = $app->{client}{app2sessions}{test};
($succ, $ret) = $app->logout();
is ($succ, 1, 'logout worked' );
is ($ret, $app, 'logout message is app');
ok ($old_sess_id ne $app->{client}{app2sessions}{test}, 'session id changed' );

($succ, $ret) = $app->login( { user => "${email}NO",
			       password => $password,
			     } );
is ($succ, 0, 'login failed' );
is ($ret, 'login failed' , 'login failed message' );

($succ, $ret) = $app->login( { user => "$email",
			       password => "${password}NO",
			     } );
is ($succ, 0, 'login failed' );
is ($ret, 'login failed' , 'login failed message' );

($succ, $ret) = $app->login( { user => $email,
			       password => $password,
			     } );
is ($succ, 1, 'login success' );
is_deeply ($ret->{data}, { login_name => 'vgroopu' }, 'login success return' );

($succ, $ret) = $app->logout();
is ($succ, 1, 'logout worked' );
is ($ret, $app, 'logout message is app');

# make some objects, see if they can be used.
($succ, $ret) = $app->produce;
is ($succ, 0, 'produce failed when not logged in' );
is ($ret, 'must be logged in to produce', 'produce result for not logged in');

($succ, $ret) = $app->login( { user => "$email",
			       password => $password,
			     } );
is ($succ, 1, 'login worked' );
is_deeply ($ret->{data}, {login_name => 'vgroopu' }, 'login stuff' );


($succ, $ret) = $app->produce;
my $obj1 = $ret;
is ($succ, 1, 'produce worked' );
is_deeply ($obj1->{data}, {}, 'produce result');

($succ, $ret) = $app->stamp( $obj1 );
is ($succ, 1, 'stamp worked' );
is_deeply ($obj1->{data}, { count => 'v1' }, 'stamp result');
is ($obj1->get_count, 1, 'stamp result with get');

($succ, $ret) = $app->produce;
my $obj2 = $ret;
is ($succ, 1, 'produce worked' );
is_deeply ($obj2->{data}, {}, 'produce result');

($succ, $ret) = $app->stamp( $obj1 );
is ($succ, 1, 'stamp worked again' );
my $info = $obj1->get_info;
my $info_arry = $obj1->get_info_arry;

my $infoid = $app->{client}->id( $info );
my $infoaid = $app->{client}->id( $info_arry );
is_deeply ($obj1->{data}, { count => 'v2', info => "r$infoid", info_arry => "r$infoaid" }, 'stamp result');
is ($obj1->get_count, 2, 'stamp result with get again');
is_deeply ($info, { got_a_stamp => '2' }, 'stamp info result with get');
is_deeply ($info_arry, [ '2' ], 'stamp info arry result with get');
is ($obj1->get_info, $info, 'info is info' );
is ($obj1->get_info->{got_a_stamp}, 2, 'info is info with a stamp' );
is_deeply ($obj1->get_info_arry, [2], 'info is info arry with a stamp' );

$app->stamp( $obj1 );
is ($obj1->get_info->{got_a_stamp}, 3, 'info is info with a stamp updated' );
is ($info->{got_a_stamp}, 3, 'info ds is info with a stamp updated' );
is_deeply( $info_arry, [ 3 ], 'info arry ds with restamped' );

($succ, $ret) = $app->store_obj( $obj1 );
is ($succ, 1, 'stored the object' );
is ($ret, $app, 'store result is app');
my $stored = $app->get_stored;

my $storedid = $app->{client}->id( $stored );
is_deeply( $stored, [ $obj1 ], 'stored list' );
is_deeply ($app->{data},
	   {
	       stored => "r$storedid",
	       special_message => 'vcan you hear',
	       message => 'vfee fi fo fum',
	   }, 'store result is app');


($succ, $ret) = $app->logout();
is ($succ, 1, 'logout worked' );
is ($ret, $app, 'logout message is app');

($succ, $ret) = $app->login( { user => "$email",
			       password => $password,
			     } );
is ($succ, 1, 'login worked' );
is_deeply ($ret->{data} , { login_name => 'vgroopu', stamp_count => 'v3' }, 'login data' );
is ($ret->get_stamp_count , 3, 'stamp count in login' );

($succ, $ret) = $app->structs;
is ($succ, 1, 'got structs' );
my $hash = $ret->[1];
is_deeply( $ret, [ $ret, $hash ], 'array contents' );
is_deeply( $hash, { arry => $ret, hash => $hash }, 'hash contents' );

my $old_stored_id = $app->{client}->id( $app->get_stored );
my $old_app_id = $app->{client}->id( $app );

($succ, $app) = $app->reset;
my $new_app_id = $app->{client}->id( $app );
is ($succ, 1, 'reset worked' );

ok ($new_app_id && $new_app_id ne $old_app_id, 'reset app' );
is ($app->{data}{special_message}, undef, 'special message reset' );
is ($app->{data}{message}, undef, 'message reset' );
ok ($app->{data}{stored} ne $old_stored_id, 'stored reset' );

($succ,$ret) = $app->stamp( $obj1 );
is ($succ, 0, 'stamp failed, no login again' );

($succ, $ret) = $app->signup( { email => $email,
				login => $login,
				password => $password,
			      } );
is ($succ, 1, 'account created' );
is (ref( $ret ), 'Yote::Client::Obj', 'login object' );
is ($ret->{app}, 'test', 'login of test app' );
is ($ret->{class}, 'Yote::App::Login', 'login of test app' );
is_deeply ($ret->{data}, { login_name => 'vgroopu' }, 'no data in logins' );
is_deeply ($ret->{methods}, {}, 'no login methods' );

$app->logout;

($succ, $ret) = $app->login( { user => $email,
			       password => $password,
			     } );
is ($succ, 1, 'login success again' );
is_deeply ($ret->{data}, {
    login_name => 'vgroopu'
	   }, 'login success return again' );

($succ,$ret) = $app->stamp( $obj1 );
is ($succ, 0, 'stamp obj1 isnt known by the session' );

($succ,$ret) = $app->show_arry( [1,2,3,4] );
is ($succ, 1, 'showed array');
is_deeply ($ret, [4], 'four array items showed' );
is ($app->get_arry_count, 4, 'four items shown in the obj');

($succ,$ret) =$app->show_arry( [5,6,7,8,9] );
is ($app->get_arry_count, 9, 'nine items now shown in the obj');
is_deeply ($ret, [9], 'nine array items showed' );

# -------------------------------------------------------------
#  uploads?

$yote->close;

done_testing;

exit;

sub prep_dir {
    my $dir = tempdir( CLEANUP => 1 );

    my $pathkey = ftok( $dir, 1 );
    my $futex_shm = IPC::SharedMem->new( $pathkey, 8, S_IRWXU | IPC_CREAT);
    $futex_shm->attach;
    $futex_shm->remove;
    $futex_shm->detach;
    $pathkey = ftok( $dir, 2 );
    my $shm = IPC::SharedMem->new( $pathkey, 8, S_IRWXU | IPC_CREAT);
    $shm->attach;
    $shm->remove;
    $shm->detach;

    return $dir;
}

