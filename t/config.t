#!/usr/bin/perl
use strict;
use warnings;

use Carp;
use File::Temp qw/ :mktemp tempdir /;
use JSON;
use YAML;

use lib '/home/wolf/open_source/recordstore/lib';
use lib '/home/wolf/open_source/objectstore/lib';
use lib './lib';
use lib 't/lib';

use Yote;
use Yote::App::Endpoint;

use Test::More;

my $dir;
sub init_test {
    my( $yaml, $keepdir) = @_;
    unless( $keepdir ) {
        $dir = tempdir( CLEANUP => 1 );
    }
    $yaml =~ s!TMPDIR!$dir/data_store!gs;
    $ENV{YOTE_ROOT_DIRECTORY} = $dir;
    open my $out, '>', "$dir/yote-conf.yaml";
    print $out "---\n$yaml\n";
    close $out;
    return Yote::App::Endpoint->init( $dir );
}

my $yote = init_test( <<"END" );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  'localhost':
    APPS:
      'pagecounter':
        MAIN-APP-CLASS: 'PageCounter'
        APP-METHODS:
          'PageCounter':
            PUBLIC:
              - fetch
END

# start with a simple config to test writing, adding the update method
$yote->domain('localhost')->get_apps->{pagecounter}->get__methods_access_levels->{PageCounter}->{PUBLIC}->{update} = 1;
$yote->write_config;
open my $in, "<", "$dir/yote-conf.yaml";
my $file = '';
while( <$in> ) {
    $file .= $_;
}
close $in;
is( $file, <<"END", "wrote file with addl method" );
---
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: $dir/data_store
DOMAINS:
  localhost:
    APPS:
      pagecounter:
        APP-METHODS:
          PageCounter:
            ADMIN-REQUIRED: []
            LOGIN-REQUIRED: []
            PUBLIC:
              - fetch
              - update
        MAIN-APP-CLASS: PageCounter
END

$yote = init_test( <<"END", 'Keep' );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  'localhost':
    APPS:
      'pagecounter':
        MAIN-APP-CLASS: 'PageCounter'
        APP-METHODS:
          'PageCounter':
            PUBLIC:
              - update_counter
END

is_deeply( $yote->domain( 'localhost' )->get_apps->{pagecounter}->get__methods_access_levels->{PageCounter}->{PUBLIC}, { update_counter => 1 }, "config update worked" );

{
    local( *STDERR );
    my $out;
    open( STDERR, ">>", \$out );
    $yote = init_test( <<"END", 'Keep' );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  ghosty:
  roasty:
END

    like( $out, qr/Removing domain 'localhost'/s, 'removed domains warning' );
    is( scalar( @{$yote->get__deleted_doms} ), 1, "One deleted domain" );
    is_deeply( [sort keys %{$yote->get_domains}], [qw(ghosty roasty)] , "Now two empty domains" );
}
delete $yote->get_domains->{ghosty};
$yote->write_config;
open $in, "<", "$dir/yote-conf.yaml";
$file = '';
while( <$in> ) {
    $file .= $_;
}
close $in;
is( $file, <<"END", "wrote cfg after deleting ghost" );
---
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: $dir/data_store
DOMAINS:
  roasty:
    APPS: {}
END


{
    local( *STDERR );
    my $out;
    open( STDERR, ">>", \$out );
    $yote = init_test( <<"END", 'Keep' );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
END

    like( $out, qr/No domains are set up/, 'no domains warning' );
}


#-------------------------------------------------
    
$yote = init_test( <<"END" );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  'localhost':
    APPS:
      'pagecounter': 
        MAIN-APP-CLASS: 'PageCounter'
        APP-METHODS:
          'PageCounter':
            PUBLIC:
               - update_counter
               - fetch
      'logintest': 
        MAIN-APP-CLASS: 'LoginTest'
        APP-METHODS:
          'PageCounter':
            PUBLIC:
               - hello
               - fetch
  'otherhost':
    APPS:
      'pagecounter': 
        MAIN-APP-CLASS: 'PageCounter'
        APP-METHODS:
          'PageCounter':
            PUBLIC:
               - fetch
      'logintest': 
        MAIN-APP-CLASS: 'LoginTest'
        APP-METHODS:
          'PageCounter':
            PUBLIC:
               - hello
               - fetch
               - echo
END


my $doms = $yote->get_domains;

is_deeply( [sort keys %$doms],
           [ qw( localhost otherhost) ], 'domains list' );


my $otherdom = $doms->{otherhost};
my $otherapps = $otherdom->get_apps;
is_deeply( [sort keys %$otherapps], [qw( logintest pagecounter ) ], "other apps" );
my $otherpage = $otherapps->{pagecounter};
is_deeply( $otherpage->get__methods_access_levels, { PageCounter => { 'ADMIN-REQUIRED' => {}, 'LOGIN-REQUIRED' => {}, PUBLIC => { fetch => 1 }} }, 'pagecounter methods' );
is_deeply( $otherapps->{logintest}->get__methods_access_levels, { PageCounter => { 'ADMIN-REQUIRED' => {}, 'LOGIN-REQUIRED' => {}, PUBLIC => { fetch => 1, hello => 1, echo => 1 }} }, 'pagecounter methods' );

eval {
    $yote = init_test( <<"END", 'Keep' );
DATA-STORE:
  CLASS: Data::RecordStore
DOMAINS:
END

    fail( "passed without config options" );
};
like( $@, qr/Missing DATA-STORE config/, 'no data store config' );

eval {
    $yote = init_test( <<"END", 'Keep' );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS: []
DOMAINS: 
END
    fail( "passed without config options" );
};
like( $@, qr/Missing DATA-STORE config/, 'no data store config' );

eval {
    $yote = init_test( <<"END", 'Keep' );
DATA-STORE:
  OPTIONS: {}
DOMAINS: 
END
    fail( "passed without config class" );
};
like( $@, qr/Missing DATA-STORE config/, 'no data store config' );

$yote = init_test( <<"END" );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  localhost:
    APPS:
      pagecounter:
        MAIN-APP-CLASS: PageCounter
        APP-METHODS:
          PageCounter:
            PUBLIC:
              - fetch
      logintest:
        MAIN-APP-CLASS: LoginTest
        LoginTest:
          PUBLIC:
            - fetch
END

is_deeply( [sort keys %{$yote->domain('localhost')->get_apps}],
           [qw( logintest pagecounter )], " set up with 2 apps" );

$yote = init_test( <<"END",'keep' );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  'localhost':
    APPS:
      'pagecounter':
        MAIN-APP-CLASS: 'PageCounter'
        APP-METHODS:
          'PageCounter':
            PUBLIC:
              - fetch
END

is_deeply( [sort keys %{$yote->domain('localhost')->get_apps}],
           ['pagecounter'], "one app remains after removal" );
is( scalar( @{$yote->domain('localhost')->get__deleted_apps} ), 1, "one removed app" );

{
    local( *STDERR );
    my $out;
    open( STDERR, ">>", \$out );
           
    $yote = init_test( <<"END",'keep' );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  'localhost':
    APPS:
      'pagecounter':
        MAIN-APP-CLASS: 'LoginTest'
        APP-METHODS:
          'PageCounter':
            PUBLIC:
              - fetch
END
    like( $out, qr/replacing domain 'localhost' app 'pagecounter'/s, "change in app class" );
    like( $out, qr/Removing app 'pagecounter'/s, 'removal warning when change in app class' );
    is_deeply( [sort keys %{$yote->domain('localhost')->get_apps}],
               ['pagecounter'], "one app remains after class change" );
    is( scalar( @{$yote->domain('localhost')->get__deleted_apps} ), 2, "two removed app" );
}

is_deeply( [sort keys %{$yote->domain('localhost')->get_apps}],
           ['pagecounter'], "one app removed" );
eval {
    local( *STDERR );
    my $out;
    open( STDERR, ">>", \$out );
    $yote = init_test( <<"END" );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  'localhost':
    APPS:
      'pagecounter':
        MAIN-APP-CLASS: 'NotApp'
        APP-METHODS:
          'NotApp':
            PUBLIC:
              - fetch
END
    fail( 'was able to install bogus app' );
};
like( $@, qr/was not able to load app 'pagecounter' of class 'NotApp'/s, "change in app class warning" );




$yote = init_test( <<"END" );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  'localhost':
    APPS:
      'pagecounter':
        MAIN-APP-CLASS: 'PageCounter'
        APP-METHODS:
          'PageCounter':
            PUBLIC:
              - fetch
END
is_deeply( [keys %{$yote->domain('localhost')->get_apps}],
           [qw(pagecounter)] , "One app, one method" );
is_deeply( $yote->domain('localhost')->get_apps->{pagecounter}->get__methods_access_levels->{PageCounter}{PUBLIC},
           { fetch => 1 }, "One app, one method" );
$yote = init_test( <<"END", 'keep' );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  'localhost':
    APPS:
      'pagecounter':
        MAIN-APP-CLASS: 'PageCounter'
        APP-METHODS:
          'PageCounter':
            PUBLIC:
              - fetch
              - update
END
is_deeply( $yote->domain('localhost')->get_apps->{pagecounter}->get__methods_access_levels->{PageCounter}{PUBLIC},
           { fetch => 1, update => 1 }, "One app, two methods" );

$yote = init_test( <<"END" );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  localhost:
    APPS:
      pagecounter:
        MAIN-APP-CLASS: PageCounter
        APP-METHODS:
          PageCounter:
            PUBLIC:
              - fetch
      logintest:
        MAIN-APP-CLASS: LoginTest
        LoginTest:
          PUBLIC:
            - fetch
END
$yote->domain('localhost')->remove_app( 'logintest' );
is( scalar( @{$yote->domain('localhost')->get__deleted_apps} ), 1, 'one deleted app via direct deletion' );
$yote->write_config;
open $in, "<", "$dir/yote-conf.yaml";
$file = '';
while( <$in> ) {
    $file .= $_;
}
close $in;
is( $file, <<"END", "wrote cfg after deleting logintest" );
---
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: $dir/data_store
DOMAINS:
  localhost:
    APPS:
      pagecounter:
        APP-METHODS:
          PageCounter:
            ADMIN-REQUIRED: []
            LOGIN-REQUIRED: []
            PUBLIC:
              - fetch
        MAIN-APP-CLASS: PageCounter
END


$yote = init_test( <<"END" );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  localhost:
    APPS:
      pagecounter:
        MAIN-APP-CLASS: 'PageCounter'
END

$yote->domain('localhost')->fetch_app( 'pagecounter' )->get__methods_access_levels->{PageCounter}{PUBLIC}{fetch} = 1;

$yote->write_config;
open $in, "<", "$dir/yote-conf.yaml";
$file = '';
while( <$in> ) {
    $file .= $_;
}
close $in;
is( $file, <<"END", "wrote cfg after deleting logintest" );
---
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: $dir/data_store
DOMAINS:
  localhost:
    APPS:
      pagecounter:
        APP-METHODS:
          PageCounter:
            ADMIN-REQUIRED: []
            LOGIN-REQUIRED: []
            PUBLIC:
              - fetch
        MAIN-APP-CLASS: PageCounter
END

$yote = init_test( <<"END" );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  localhost:
    APPS:
      pagecounter:
        APP-METHODS:
          PageCounter:
            PUBLIC:
              - fetch
              - update
        MAIN-APP-CLASS: 'PageCounter'
END

$yote = init_test( <<"END" );
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: TMPDIR
DOMAINS:
  localhost:
    APPS:
      pagecounter:
        APP-METHODS:
          PageCounter:
            PUBLIC:
              - fetch
              - update
          Widget:
            PUBLIC:
              - fetch
              - update
        MAIN-APP-CLASS: 'PageCounter'
END

delete $yote->domain('localhost')->fetch_app( 'pagecounter' )->get__methods_access_levels->{Widget};

$yote->write_config;
open $in, "<", "$dir/yote-conf.yaml";
$file = '';
while( <$in> ) {
    $file .= $_;
}
close $in;
is( $file, <<"END", "wrote cfg after deleting logintest" );
---
DATA-STORE:
  CLASS: Data::RecordStore
  OPTIONS:
    BASE_PATH: $dir/data_store
DOMAINS:
  localhost:
    APPS:
      pagecounter:
        APP-METHODS:
          PageCounter:
            ADMIN-REQUIRED: []
            LOGIN-REQUIRED: []
            PUBLIC:
              - fetch
              - update
        MAIN-APP-CLASS: PageCounter
END

done_testing;

